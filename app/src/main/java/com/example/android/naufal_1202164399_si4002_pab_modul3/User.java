package com.example.android.naufal_1202164399_si4002_pab_modul3;

class User {

    private String nama;
    private String pekerjaan;
    private final int avatar;

    public User(String nama, String pekerjaan, int avatar) {
        this.nama = nama;
        this.pekerjaan = pekerjaan;
        this.avatar = avatar;
    }

    public String getNama() {
        return nama;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public int getAvatar() {
        return avatar;
    }
}
