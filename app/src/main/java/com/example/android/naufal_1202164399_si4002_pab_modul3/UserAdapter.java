package com.example.android.naufal_1202164399_si4002_pab_modul3;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;




public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {
    private ArrayList<User> daftarUser;
    private Context mContext;

    public UserAdapter(ArrayList<User> daftarUser, Context mContext) {
        this.daftarUser = daftarUser;
        this.mContext = mContext;
    }

    @Override
    public UserAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(mContext).
                inflate(R.layout.list_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(UserAdapter.ViewHolder viewHolder, int urutan) {
        User currentUser = daftarUser.get(urutan);
        viewHolder.bindTo(currentUser);
    }

    @Override
    public int getItemCount() {
        return daftarUser.size();
    }



    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView nama, pekerjaan;
        private ImageView foto;
        private int avatarCode;

        public ViewHolder(View itemView) {
            super(itemView);

            nama = itemView.findViewById(R.id.tx_nama);
            pekerjaan = itemView.findViewById(R.id.tx_pekerjaan);
            foto = itemView.findViewById(R.id.avatar);

            itemView.setOnClickListener(this);
        }

        void bindTo(User currentUser){
            nama.setText(currentUser.getNama());
            pekerjaan.setText(currentUser.getPekerjaan());

            avatarCode = currentUser.getAvatar();
            switch (currentUser.getAvatar()){
                case 1 :
                    foto.setImageResource(R.drawable.man);
                    break;
                case 2 :
                default:
                    foto.setImageResource(R.drawable.woman);
                    break;
            }
        }

        @Override
        public void onClick(View v) {
            Intent toDetailActivity = new Intent(v.getContext(),ActivityDetail.class);
            toDetailActivity.putExtra("nama",nama.getText().toString());
            toDetailActivity.putExtra("gender",avatarCode);
            toDetailActivity.putExtra("pekerjaan",pekerjaan.getText().toString());
            v.getContext().startActivity(toDetailActivity);
        }
    }

}
